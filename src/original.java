
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import javax.swing.JOptionPane;

/**
 *
 * Dada uma frase, o programa deve ser capaz de cifrar (criptografar) e decifrar
 * (descriptografar) usando um esquema de matrizes.
 *
 * Suponha uma frase de N caracteres. Será gerada uma matriz quadrada de tamanho
 * igual ao inteiro superior mais próximo à raiz quadrada de N.
 * 
 * Por exemplo, para a frase:
 * 
 *      O rato roeu a roupa do rei de Roma
 * 
 * Será criada uma matriz 6x6, pois há 34 caracteres.
 * 
 * Os caracteres da frase serão então distribuídos nas linhas da matriz da 
 * esquerda para a direita, de cima para baixo (espaços vazios do final são
 * preenchidos com espaços):
 * 
 *      O rato
 *       roeu 
 *      a roup 
 *      a do r
 *      ei de 
 *      Roma  

        O rato
         roeu 
        a roup
        a do r
        ei de 
        Roma  
 *
 * A matriz resultante é então espelhada horizontalmente:
 * 
 *      Roma
 *      ei de 
 *      a do r
 *      a roup 
 *       roeu 
 *      O rato
 * 
 * A frase cifrada é criada a partir da leitura das colunas da matriz de cima
 * para baixo, da direita para a esquerda (em um processo de serialização que
 * também remove espaços do final da frase se houver):
 * 
 *      Reaa Ooi  r m droradooea e uut  rp o
 * 
 * A mensagem é então mostrada para o usuário no terminal ou painéis de diálogo.
 * 
 * Para decifrar, o programa monta a matriz com base nas colunas, espelha
 * novamente e serializa o resultado com base nas linhas.
 * 
 * OBS.: o programa deve ler a entrada a partir de um arquivo de texto para 
 * cifragem e decifrar logo em seguida uma.
 *
 */
public class original {
    
    public static void main(String[] args) throws IOException {
//        String arquivo = JOptionPane.showInputDialog("Informe o nome do arquivo com os dados:");
        String arquivo = "/home/m4/NetBeansProjects/CifradorDecifrador/src/arquivo";
        String frase = leArquivo(arquivo);
        char[][] matriz = cifra(frase);
        matriz = espelha(matriz);
        frase = serializaCifra(matriz);
        matriz = decifra(frase);
        matriz = espelha(matriz);
        frase = serializaDecifra(matriz);
    }
    
    public static String leArquivo(String caminho) throws FileNotFoundException, IOException{
        FileReader fileReader = new FileReader(caminho);
        BufferedReader reader = new BufferedReader(fileReader);
        String texto = reader.readLine();
        fileReader.close();
        reader.close();
        return texto;
    }
    
    public static char[][] cifra(String frase){
        int tamanho = (int) Math.ceil(Math.sqrt(frase.length()));
        char[][] matriz = new char[tamanho][tamanho];
        for(int i=0; i<tamanho; i++){
            for(int j=0; j<tamanho; j++){
                try{
                    matriz[i][j] = frase.charAt(j*tamanho+i);
                }catch(IndexOutOfBoundsException e){
                    matriz[i][j] = ' ';
                }
            }
        }
        return matriz;
    }
        
    public static String serializaCifra(char[][] matriz){
        String frase = "";
        
        for(int i=0; i<matriz.length; i++){
            for(int j=0; j<matriz[i].length; j++){
                frase += matriz[j][i];
                frase = frase.trim();
            }
        }
       
        return frase;
    }
    
    public static char[][] decifra(String frase){
        int tamanho = (int) Math.ceil(Math.sqrt(frase.length()));
        char[][] matriz = new char[tamanho][tamanho];
        for(int i=0; i<tamanho; i++){
            for(int j=0; j<tamanho; j++){
                try{
                    matriz[j][i] = frase.charAt(j*tamanho+i);
                }catch(IndexOutOfBoundsException e){
                    matriz[j][i] = ' ';
                }
            }
        }
        return matriz;
    }
    
    public static String serializaDecifra(char[][] matriz){
        String frase = "";
        
        for(int i=0; i<matriz.length; i++){
            for(int j=0; j<matriz[i].length; j++){
                frase += matriz[i][j];
                frase = frase.trim();
            }
        }
        
        return frase;
    }   
    
    
    public static char[][] espelha(char[][] matriz){
        char[] temp = new char[matriz.length];
        
        for(int i=0; i<matriz.length/2; i++){
            for(int j=0; j<matriz[0].length; j++){
                temp[j] = matriz[i][j];
                matriz[i][j] = matriz[matriz[i].length - i][j];
                matriz[matriz[i].length - i][j] = temp[j];
            }
        }
        
        return matriz;
    }
}
