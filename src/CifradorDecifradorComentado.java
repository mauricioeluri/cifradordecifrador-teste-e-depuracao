
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import javax.swing.JOptionPane;

/**
 *
 * Dada uma frase, o programa deve ser capaz de cifrar (criptografar) e decifrar
 * (descriptografar) usando um esquema de matrizes.
 *
 * Suponha uma frase de N caracteres. Será gerada uma matriz quadrada de tamanho
 * igual ao inteiro superior mais próximo à raiz quadrada de N.
 *
 * Por exemplo, para a frase:
 *
 * O rato roeu a roupa do rei de Roma
 *
 * Será criada uma matriz 6x6, pois há 34 caracteres.
 *
 * Os caracteres da frase serão então distribuídos nas linhas da matriz da
 * esquerda para a direita, de cima para baixo (espaços vazios do final são
 * preenchidos com espaços):
 *
 * O rato roeu a roup a do r ei de Roma
 *
 * A matriz resultante é então espelhada horizontalmente:
 *
 * Roma ei de a do r a roup roeu O rato
 *
 * A frase cifrada é criada a partir da leitura das colunas da matriz de cima
 * para baixo, da direita para a esquerda (em um processo de serialização que
 * também remove espaços do final da frase se houver):
 *
 * Reaa Ooi r m droradooea e uut rp o
 *
 * A mensagem é então mostrada para o usuário no terminal ou painéis de diálogo.
 *
 * Para decifrar, o programa monta a matriz com base nas colunas, espelha
 * novamente e serializa o resultado com base nas linhas.
 *
 * OBS.: o programa deve ler a entrada a partir de um arquivo de texto para
 * cifragem e decifrar logo em seguida uma.
 *
 */
public class CifradorDecifradorComentado {

    public static void main(String[] args) throws IOException {
//        String arquivo = JOptionPane.showInputDialog("Informe o nome do arquivo com os dados:");
        String arquivo = "/home/m4/NetBeansProjects/CifradorDecifrador/src/arquivo";
        String frase = leArquivo(arquivo);
        char[][] matriz = cifra(frase);
        matriz = espelha(matriz);
        frase = serializaCifra(matriz);
        System.out.println("Frase cifrada: " + frase);
        matriz = decifra(frase);
//                espelha novamente
        matriz = espelha(matriz);
//    serializa o resultado com base nas linhas.
        frase = serializaDecifra(matriz);
        System.out.println("Frase decifrada: " + frase);
    }

    /**
     * Recebe uma string com o caminho e nome do arquivo e retorna uma String
     * com o conteúdo do arquivo.
     *
     * @param caminho
     * @return
     * @throws FileNotFoundException
     * @throws IOException
     */
    public static String leArquivo(String caminho) throws FileNotFoundException, IOException {
        FileReader fileReader = new FileReader(caminho);
        BufferedReader reader = new BufferedReader(fileReader);
        String texto = reader.readLine();
        fileReader.close();
        reader.close();
        return texto;
    }

    /**
     * Suponha uma frase de N caracteres. Será gerada uma matriz quadrada de
     * tamanho igual ao inteiro superior mais próximo à raiz quadrada de N.
     *
     * Por exemplo, para a frase:
     *
     * O rato roeu a roupa do rei de Roma Será criada uma matriz 6x6, pois há 34
     * caracteres.
     *
     * Os caracteres da frase serão então distribuídos nas linhas da matriz da
     * esquerda para a direita, de cima para baixo (espaços vazios do final são
     * preenchidos com espaços):
     *
     * O rato roeu a roup a do r ei de Roma
     *
     * @param frase
     * @return
     */
    public static char[][] cifra(String frase) {
//      Math.ceil() -> retorna um número inteiro maior ou igual ao número recebido.
//      Se o valor já for um inteiro, ele retorna o próprio número.
//      Recebe double, retorna double.

//      Math.sqrt() -> retorna a raiz quadrada do número recebido.
//      tamanho será um inteiro de tamanho igual ou superior a raiz quadrada
//      do tamanho da frase.
        int tamanho = (int) Math.ceil(Math.sqrt(frase.length()));

        char[][] matriz = new char[tamanho][tamanho];

//      Preenche a matriz de caracteres. Agora vamos entender como... :]
        for (int linha = 0; linha < tamanho; linha++) {
            for (int coluna = 0; coluna < tamanho; coluna++) {
                try {
//      charAt(x) -> retorna o caractere localizado na posição x de uma String

//CRIA A MATRIZ. EX:
//        iiiiii
//     j  O rato
//     j   roeu 
//     j  a roup 
//     j  a do r
//     j  ei de 
//     j  Roma
                    matriz[linha][coluna] = frase.charAt(linha * tamanho + coluna);
                } catch (IndexOutOfBoundsException e) {
                    //se o tamanho da matriz exceder o limite adiciona um espaço no lugar
                    matriz[linha][coluna] = ' ';
                }
            }
        }
        return matriz;
    }

    /**
     *
     * @param matriz
     * @return
     */
    public static String serializaCifra(char[][] matriz) {
        String frase = "";

        for (int i = 0; i < matriz.length; i++) {
            for (int j = 0; j < matriz[i].length; j++) {
                frase += matriz[j][i];
            }
        }

        return frase.trim();
    }

    /**
     * Para decifrar, o programa monta a matriz com base nas colunas
     * @param frase
     * @return
     */
    public static char[][] decifra(String frase) {
        int tamanho = (int) Math.ceil(Math.sqrt(frase.length()));
        char[][] matriz = new char[tamanho][tamanho];
        for (int i = 0; i < tamanho; i++) {
            for (int j = 0; j < tamanho; j++) {
                try {
                    matriz[j][i] = frase.charAt(i * tamanho + j);
                } catch (IndexOutOfBoundsException e) {
                    matriz[j][i] = ' ';
                }
            }
        }
        return matriz;
    }

    /**
     *
     * @param matriz
     * @return
     */
    public static String serializaDecifra(char[][] matriz) {
        String frase = "";

        for (int i = 0; i < matriz.length; i++) {
            for (int j = 0; j < matriz[i].length; j++) {
                frase += matriz[i][j];
            }
        }

        return frase.trim();
    }

//      A matriz resultante é então espelhada horizontalmente:
//    Roma
//    ei de 
//    a do r
//    a roup 
//     roeu 
//    O rato
    /**
     *
     *
     *
     *
     * @param matriz
     * @return
     */
    public static char[][] espelha(char[][] matriz) {
        char[] temp = new char[matriz.length];

//        PERCORRE AS LINHAS ATÉ O TAMANHO DO ARRAY
//        (dividido por 2 pois o length retorna o tamanho vertical e horizontal,
//         como o array é x*x dividindo por 2 retorna o valor de uma posição).
//        estranho mas funciona...
//        porém a linha vai só até a metade
//      ex: (6) -> 0 1 2
//        System.out.print(matriz[0].length);
        for (int linha = 0; linha < matriz.length / 2; linha++) {
            //PERCORRE TODAS AS COLUNAS ATÉ A METADE DO ARRAY
            for (int coluna = 0; coluna < matriz[0].length; coluna++) {
//        matriz[0].length mostra o tamanho TOTAL do array
                //pega o valor da coluna
                temp[coluna] = matriz[linha][coluna];
//                matriz[linha].length é a mesma coisa... Só pra complicar
//                retorna o tamanho total do array

//                System.out.print(matriz[linha][coluna]);
//            O ARRAY TA INDO ATÉ A METADE!!
//                System.out.println(matriz[linha].length - (linha + 1) + " " + coluna );
                matriz[linha][coluna] = matriz[matriz[linha].length-1 - linha][coluna];
                matriz[matriz[linha].length-1 - linha][coluna] = temp[coluna];
            }
        }

        return matriz;
    }
}
