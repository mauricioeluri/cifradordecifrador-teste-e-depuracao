/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.FileNotFoundException;
//import org.junit.Assert;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Maurício El Uri
 */
public class CifradorDecifradorCorrigidoTest {

    public CifradorDecifradorCorrigidoTest() {
    }

//    /**
//     * Test of main method, of class CifradorDecifradorCorrigido.
//     */
//    @Test
//    public void testMain() throws Exception {
//        System.out.println("main");
//        String[] args = null;
//        CifradorDecifradorCorrigido.main(args);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
    /**
     * Test of leArquivo method, of class CifradorDecifradorCorrigido.
     */
    @Test
    public void testLeArquivoValido() throws Exception {
        String caminho = "/home/m4/NetBeansProjects/CifradorDecifrador/src/arquivo";
        String expResult = "O rato roeu a roupa do rei de Roma";
        String result = CifradorDecifradorCorrigido.leArquivo(caminho);
        assertEquals(expResult, result);
    }

    @Test(expected = FileNotFoundException.class)
    public void testLeArquivoInvalido() throws Exception {
        String caminho = "/home/m4/NetBeansProjects/CifradorDecifrador/src/arquivod";
        CifradorDecifradorCorrigido.leArquivo(caminho);
    }

    @Test(expected = FileNotFoundException.class)
    public void testLeArquivoEspacos() throws Exception {
        String caminho = "  ";
        CifradorDecifradorCorrigido.leArquivo(caminho);
    }

    @Test(expected = FileNotFoundException.class)
    public void testLeArquivoCaracteresEspeciais() throws Exception {
        String caminho = "/%*";
        CifradorDecifradorCorrigido.leArquivo(caminho);
    }

    /**
     * Test of cifra method, of class CifradorDecifradorCorrigido.
     */
    @Test
    public void testCifra() {
        String frase = "O rato roeu a roupa do rei de Roma";
        int i = 0;
        char[][] expResult = new char[6][6];
        for (int x = 0; x < 6; x++) {
            for (int y = 0; y < 6; y++) {
                try {
                    expResult[x][y] = frase.charAt(i);
                } catch (IndexOutOfBoundsException e) {
                    expResult[x][y] = ' ';
                }
                i++;
            }
        }
        char[][] result = CifradorDecifradorCorrigido.cifra(frase);
        assertArrayEquals(expResult, result);
    }

    @Test
    public void testCifra2() {
        String frase = "O rei roeu a roupa do R@to de roma";
        int i = 0;
        char[][] expResult = new char[6][6];
        for (int x = 0; x < 6; x++) {
            for (int y = 0; y < 6; y++) {
                try {
                    expResult[x][y] = frase.charAt(i);
                } catch (IndexOutOfBoundsException e) {
                    expResult[x][y] = ' ';
                }
                i++;
            }
        }
        char[][] result = CifradorDecifradorCorrigido.cifra(frase);
        assertArrayEquals(expResult, result);
    }

    @Test
    public void testCifra3() {
        String frase = "mauricio";
        int i = 0;
        char[][] expResult = new char[3][3];
        for (int x = 0; x < 3; x++) {
            for (int y = 0; y < 3; y++) {
                try {
                    expResult[x][y] = frase.charAt(i);
                } catch (IndexOutOfBoundsException e) {
                    expResult[x][y] = ' ';
                }
                i++;
            }
        }
        char[][] result = CifradorDecifradorCorrigido.cifra(frase);
        assertArrayEquals(expResult, result);
    }

    @Test
    public void testCifra4() {
        String frase = "";
        char[][] expResult = new char[0][0];
        char[][] result = CifradorDecifradorCorrigido.cifra(frase);
        assertArrayEquals(expResult, result);
    }

    @Test
    public void testCifraQuote() {
        String frase = "Innovation distinguishes between a leader and a follower. - Steve Jobs";
        int i = 0;
        char[][] expResult = new char[9][9];
        for (int x = 0; x < 9; x++) {
            for (int y = 0; y < 9; y++) {
                try {
                    expResult[x][y] = frase.charAt(i);
                } catch (IndexOutOfBoundsException e) {
                    expResult[x][y] = ' ';
                }
                i++;
            }
        }
        char[][] result = CifradorDecifradorCorrigido.cifra(frase);
        assertArrayEquals(expResult, result);
    }

    @Test
    public void testCifraEspacos() {
        String frase = "         ";
        int i = 0;
        char[][] expResult = new char[3][3];
        for (int x = 0; x < 3; x++) {
            for (int y = 0; y < 3; y++) {
                try {
                    expResult[x][y] = frase.charAt(i);
                } catch (IndexOutOfBoundsException e) {
                    expResult[x][y] = ' ';
                }
                i++;
            }
        }
        char[][] result = CifradorDecifradorCorrigido.cifra(frase);
        assertArrayEquals(expResult, result);
    }

    /**
     * Test of serializaCifra method, of class CifradorDecifradorCorrigido.
     */
    @Test
    public void testSerializaCifra() {        
////    Roma
////    ei de 
////    a do r
////    a roup 
////     roeu 
////    O rato 
        String frase = "Roma  ei de a do ra roup roeu O rato";
        int i = 0;
        char[][] matriz = new char[6][6];
        for (int x = 0; x < 6; x++) {
            for (int y = 0; y < 6; y++) {
                try {
                    matriz[x][y] = frase.charAt(i);
                } catch (IndexOutOfBoundsException e) {
                    matriz[x][y] = ' ';
                }
                i++;
            }
        }
        String expResult = "Reaa Ooi  r m droradooea e uut  rp o";
        String result = CifradorDecifradorCorrigido.serializaCifra(matriz);
        assertEquals(expResult, result);
    }
    /**
     * Test of decifra method, of class CifradorDecifradorCorrigido.
     */
    @Test
    public void testDecifra() {
        String frase = "Reaa Ooi  r m droradooea e uut  rp o";
        int i = 0;
        char[][] expResult = new char[6][6];
        for (int x = 0; x < 6; x++) {
            for (int y = 0; y < 6; y++) {
                try {
                    expResult[y][x] = frase.charAt(i);
                } catch (IndexOutOfBoundsException e) {
                    expResult[y][x] = ' ';
                }
                i++;
            }
        }
        char[][] result = CifradorDecifradorCorrigido.decifra(frase);
        assertArrayEquals(expResult, result);
    }

    @Test
    public void testDecifraEspacos() {
        String frase = "              d                     ";
        int i = 0;
        char[][] expResult = new char[6][6];
        for (int x = 0; x < 6; x++) {
            for (int y = 0; y < 6; y++) {
                try {
                    expResult[y][x] = frase.charAt(i);
                } catch (IndexOutOfBoundsException e) {
                    expResult[y][x] = ' ';
                }
                i++;
            }
        }
        char[][] result = CifradorDecifradorCorrigido.decifra(frase);
        assertArrayEquals(expResult, result);
    }

    @Test
    public void testDecifraNumeros() {
        String frase = "9081";
        int i = 0;
        char[][] expResult = new char[2][2];
        for (int x = 0; x < 2; x++) {
            for (int y = 0; y < 2; y++) {
                try {
                    expResult[y][x] = frase.charAt(i);
                } catch (IndexOutOfBoundsException e) {
                    expResult[y][x] = ' ';
                }
                i++;
            }
        }
        char[][] result = CifradorDecifradorCorrigido.decifra(frase);
        assertArrayEquals(expResult, result);
    }

    @Test
    public void testDecifraCaracteresEspeciais() {
        String frase = "/**&_@#**/";
        int i = 0;
        char[][] expResult = new char[4][4];
        for (int x = 0; x < 4; x++) {
            for (int y = 0; y < 4; y++) {
                try {
                    expResult[y][x] = frase.charAt(i);
                } catch (IndexOutOfBoundsException e) {
                    expResult[y][x] = ' ';
                }
                i++;
            }
        }
        char[][] result = CifradorDecifradorCorrigido.decifra(frase);
        assertArrayEquals(expResult, result);
    }

    /**
     * Test of serializaDecifra method, of class CifradorDecifradorCorrigido.
     */
    @Test
    public void testSerializaDecifra() {
        //CIFRA A FRASE DO MESMO MODO QUE O testCifra()
        String frase = "O rato roeu a roupa do rei de Roma";
        int i = 0;
        char[][] matriz = new char[6][6];
        for (int x = 0; x < 6; x++) {
            for (int y = 0; y < 6; y++) {
                try {
                    matriz[x][y] = frase.charAt(i);
                } catch (IndexOutOfBoundsException e) {
                    matriz[x][y] = ' ';
                }
                i++;
            }
        }
        String result = CifradorDecifradorCorrigido.serializaDecifra(matriz);
        assertEquals(frase, result);
    }

    @Test
    public void testSerializaDecifra2() {
        //CIFRA A FRASE DO MESMO MODO QUE O testCifra2()
        String frase = "O rei roeu a roupa do R@to de roma";
        int i = 0;
        char[][] matriz = new char[6][6];
        for (int x = 0; x < 6; x++) {
            for (int y = 0; y < 6; y++) {
                try {
                    matriz[x][y] = frase.charAt(i);
                } catch (IndexOutOfBoundsException e) {
                    matriz[x][y] = ' ';
                }
                i++;
            }
        }
        String result = CifradorDecifradorCorrigido.serializaDecifra(matriz);
        assertEquals(frase, result);
    }

    @Test
    public void testSerializaDecifra3() {
        //CIFRA A FRASE DO MESMO MODO QUE O testCifra3()
        String frase = "mauricio";
        int i = 0;
        char[][] matriz = new char[3][3];
        for (int x = 0; x < 3; x++) {
            for (int y = 0; y < 3; y++) {
                try {
                    matriz[x][y] = frase.charAt(i);
                } catch (IndexOutOfBoundsException e) {
                    matriz[x][y] = ' ';
                }
                i++;
            }
        }
        String result = CifradorDecifradorCorrigido.serializaDecifra(matriz);
        assertEquals(frase, result);
    }

    @Test
    public void testSerializaDecifra4() {
        //CIFRA A FRASE DO MESMO MODO QUE O testCifra4()
        String frase = "";
        char[][] matriz = new char[0][0];

        String result = CifradorDecifradorCorrigido.serializaDecifra(matriz);
        assertEquals(frase, result);
    }

    /**
     * Test of espelha method, of class CifradorDecifradorCorrigido.
     */
    @Test
    public void testEspelha() {
        char[][] matriz = new char[2][2];
        matriz[0][0] = '1';
        matriz[0][1] = '2';
        matriz[1][0] = '3';
        matriz[1][1] = '4';
        char[][] expResult = new char[2][2];
        expResult[0][0] = '3';
        expResult[0][1] = '4';
        expResult[1][0] = '1';
        expResult[1][1] = '2';
        char[][] result = CifradorDecifradorCorrigido.espelha(matriz);
        assertArrayEquals(expResult, result);
    }
}
