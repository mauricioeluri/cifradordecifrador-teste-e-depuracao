import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Maurício El Uri
 */
public class CifradorDecifradorTest {
    
    public CifradorDecifradorTest() {
    }

    /**
     * Test of main method, of class CifradorDecifrador.
     */
//    @Test
//    public void testMain() throws Exception {
//        System.out.println("main");
//        String[] args = "s";
//        CifradorDecifrador.main();
        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }

    /**
     * Test of leArquivo method, of class CifradorDecifrador.
     */
//    @Test
//    public void testLeArquivo() throws Exception {
//        System.out.println("leArquivo");
//        String caminho = "";
//        String expResult = "";
//        String result = CifradorDecifrador.leArquivo(caminho);
//        assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }

    /**
     * Test of cifra method, of class CifradorDecifrador.
     */
//    @Test
//    public void testCifra() {
//        String frase = "O rato roeu a roupa do rei de Roma";
//        char[][] expResult = new char[6][6];
//        expResult[0][0] = 'O'; expResult[0][1] = ' '; expResult[0][2] = 'r'; expResult[0][3] = 'a'; expResult[0][4] = 't'; expResult[0][5] = 'o';
//        expResult[1][0] = ' '; expResult[1][1] = 'r'; expResult[1][2] = 'o'; expResult[1][3] = 'e'; expResult[1][4] = 'u'; expResult[1][5] = ' ';
//        expResult[2][0] = 'a'; expResult[2][1] = ' '; expResult[2][2] = 'r'; expResult[2][3] = 'o'; expResult[2][4] = 'u'; expResult[2][5] = 'p';
//        expResult[3][0] = 'a'; expResult[3][1] = ' '; expResult[3][2] = 'd'; expResult[3][3] = 'o'; expResult[3][4] = ' '; expResult[3][5] = 'r';
//        expResult[4][0] = 'e'; expResult[4][1] = 'i'; expResult[4][2] = ' '; expResult[4][3] = 'd'; expResult[4][4] = 'e'; expResult[4][5] = ' ';
//        expResult[5][0] = 'R'; expResult[5][1] = 'o'; expResult[5][2] = 'm'; expResult[5][3] = 'a'; expResult[5][4] = ' '; expResult[5][5] = ' ';
//        char[][] result = CifradorDecifrador.cifra(frase);
//        System.out.println(result[0][0] + "" + result[0][1] + "" + result[0][2] + "" + result[0][3] + "" + result[0][4] + "" + result[0][5]);
//        System.out.println(result[1][0] + "" + result[1][1] + "" + result[1][2] + "" + result[1][3] + "" + result[1][4] + "" + result[1][5]);
//        System.out.println(result[2][0] + "" + result[2][1] + "" + result[2][2] + "" + result[2][3] + "" + result[2][4] + "" + result[2][5]);
//        System.out.println(result[3][0] + "" + result[3][1] + "" + result[3][2] + "" + result[3][3] + "" + result[3][4] + "" + result[3][5]);
//        System.out.println(result[4][0] + "" + result[4][1] + "" + result[4][2] + "" + result[4][3] + "" + result[4][4] + "" + result[4][5]);
//        System.out.println(result[5][0] + "" + result[5][1] + "" + result[5][2] + "" + result[5][3] + "" + result[5][4] + "" + result[5][5]);
//        assertArrayEquals(expResult, result);
//    }

    /**
     * Test of serializaCifra method, of class CifradorDecifrador.
     */
//    @Test
//    public void testSerializaCifra() {
//        System.out.println("serializaCifra");
//        char[][] matriz = null;
//        String expResult = "";
//        String result = CifradorDecifrador.serializaCifra(matriz);
//        assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }

    /**
     * Test of decifra method, of class CifradorDecifrador.
     */
    @Test
    public void testDecifra() {
//        System.out.println("decifra");
        String frase = "Reaa Ooi  r m droradooea e uut  rp o";
        char[][] expResult = null;
        char[][] result = CifradorDecifradorCorrigido.decifra(frase);
        System.out.println(result[0][0] + "" + result[0][1] + "" + result[0][2] + "" + result[0][3] + "" + result[0][4] + "" + result[0][5]);
        System.out.println(result[1][0] + "" + result[1][1] + "" + result[1][2] + "" + result[1][3] + "" + result[1][4] + "" + result[1][5]);
        System.out.println(result[2][0] + "" + result[2][1] + "" + result[2][2] + "" + result[2][3] + "" + result[2][4] + "" + result[2][5]);
        System.out.println(result[3][0] + "" + result[3][1] + "" + result[3][2] + "" + result[3][3] + "" + result[3][4] + "" + result[3][5]);
        System.out.println(result[4][0] + "" + result[4][1] + "" + result[4][2] + "" + result[4][3] + "" + result[4][4] + "" + result[4][5]);
        System.out.println(result[5][0] + "" + result[5][1] + "" + result[5][2] + "" + result[5][3] + "" + result[5][4] + "" + result[5][5]);        
        assertArrayEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
    }

    /**
     * Test of serializaDecifra method, of class CifradorDecifrador.
     */
//    @Test
//    public void testSerializaDecifra() {
//        System.out.println("serializaDecifra");
//        char[][] matriz = null;
//        String expResult = "";
//        String result = CifradorDecifrador.serializaDecifra(matriz);
//        assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }

    /**
     * Test of espelha method, of class CifradorDecifrador.
     */
//    @Test
//    public void testEspelha() {
////        char[][] matriz = null;
//        
//        char[][] matriz = new char[6][6];
//        matriz[0][0] = 'O'; matriz[0][1] = ' '; matriz[0][2] = 'r'; matriz[0][3] = 'a'; matriz[0][4] = 't'; matriz[0][5] = 'o';
//        matriz[1][0] = ' '; matriz[1][1] = 'r'; matriz[1][2] = 'o'; matriz[1][3] = 'e'; matriz[1][4] = 'u'; matriz[1][5] = ' ';
//        matriz[2][0] = 'a'; matriz[2][1] = ' '; matriz[2][2] = 'r'; matriz[2][3] = 'o'; matriz[2][4] = 'u'; matriz[2][5] = 'p';
//        matriz[3][0] = 'a'; matriz[3][1] = ' '; matriz[3][2] = 'd'; matriz[3][3] = 'o'; matriz[3][4] = ' '; matriz[3][5] = 'r';
//        matriz[4][0] = 'e'; matriz[4][1] = 'i'; matriz[4][2] = ' '; matriz[4][3] = 'd'; matriz[4][4] = 'e'; matriz[4][5] = ' ';
//        matriz[5][0] = 'R'; matriz[5][1] = 'o'; matriz[5][2] = 'm'; matriz[5][3] = 'a'; matriz[5][4] = ' '; matriz[5][5] = ' ';
//        
////    Roma
////    ei de 
////    a do r
////    a roup 
////     roeu 
////    O rato 
//        char[][] expResult = new char[6][6];
//        expResult[0][0] = 'R'; expResult[0][1] = 'o'; expResult[0][2] = 'm'; expResult[0][3] = 'a'; expResult[0][4] = ' '; expResult[0][5] = ' ';
//        expResult[1][0] = 'e'; expResult[1][1] = 'i'; expResult[1][2] = ' '; expResult[1][3] = 'd'; expResult[1][4] = 'e'; expResult[1][5] = ' ';
//        expResult[2][0] = 'a'; expResult[2][1] = ' '; expResult[2][2] = 'd'; expResult[2][3] = 'o'; expResult[2][4] = ' '; expResult[2][5] = 'r';
//        expResult[3][0] = 'a'; expResult[3][1] = ' '; expResult[3][2] = 'r'; expResult[3][3] = 'o'; expResult[3][4] = 'u'; expResult[3][5] = 'p';
//        expResult[4][0] = ' '; expResult[4][1] = 'r'; expResult[4][2] = 'o'; expResult[4][3] = 'e'; expResult[4][4] = 'u'; expResult[4][5] = ' ';
//        expResult[5][0] = 'O'; expResult[5][1] = ' '; expResult[5][2] = 'r'; expResult[5][3] = 'a'; expResult[5][4] = 't'; expResult[5][5] = 'o';
//        char[][] result = CifradorDecifrador.espelha(matriz);
//        
//        System.out.println(result[0][0] + "" + result[0][1] + "" + result[0][2] + "" + result[0][3] + "" + result[0][4] + "" + result[0][5]);
//        System.out.println(result[1][0] + "" + result[1][1] + "" + result[1][2] + "" + result[1][3] + "" + result[1][4] + "" + result[1][5]);
//        System.out.println(result[2][0] + "" + result[2][1] + "" + result[2][2] + "" + result[2][3] + "" + result[2][4] + "" + result[2][5]);
//        System.out.println(result[3][0] + "" + result[3][1] + "" + result[3][2] + "" + result[3][3] + "" + result[3][4] + "" + result[3][5]);
//        System.out.println(result[4][0] + "" + result[4][1] + "" + result[4][2] + "" + result[4][3] + "" + result[4][4] + "" + result[4][5]);
//        System.out.println(result[5][0] + "" + result[5][1] + "" + result[5][2] + "" + result[5][3] + "" + result[5][4] + "" + result[5][5]);
//        
//        assertArrayEquals(expResult, result);
//    }
    
}
